import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gudang_sepatu/base/base_theme.dart';
import 'package:gudang_sepatu/ui/main_menu.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.white));
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          fontFamily: "Rubik",
          primarySwatch: BaseTheme.primaryColor,
          scaffoldBackgroundColor: Colors.white,
          bottomNavigationBarTheme:
              BottomNavigationBarThemeData(backgroundColor: Colors.black)),
      home: MainMenu(),
    );
  }
}
