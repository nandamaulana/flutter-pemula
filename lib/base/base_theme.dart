import 'package:flutter/material.dart';

class BaseTheme {
  static const int _primaryColorValue = 0xFF6371F2;
  static const MaterialColor primaryColor =
      MaterialColor(_primaryColorValue, <int, Color>{
    50: Color(0xFFE3F2FD),
    100: Color(0xFFBBDEFB),
    200: Color(0xFF90CAF9),
    300: Color(0xFF64B5F6),
    400: Color(0xFF42A5F5),
    900: Color(0xFF0D47A1),
    600: Color(0xFF1E88E5),
    700: Color(0xFF1976D2),
    800: Color(0xFF1565C0),
    500: Color(_primaryColorValue)
  });

  static const Color grayChateau = Color(0xFF9AA5B1);
  static const Color oxfordBlue = Color(0xFF323F4B);
}
