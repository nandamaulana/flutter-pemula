import 'package:flutter/material.dart';
import 'package:gudang_sepatu/base/base_theme.dart';

class ShoesCardItem extends StatelessWidget {
  const ShoesCardItem(
      {Key? key,
      required this.image,
      required this.title,
      required this.price,
      required this.onClick})
      : super(key: key);
  final String image;
  final String title;
  final String price;
  final Function() onClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        enableFeedback: false,
        mouseCursor: SystemMouseCursors.click,
        onTap: () {
          onClick.call();
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                height: 150,
                width: 150,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(image)))),
            Container(
              margin: EdgeInsets.only(top: 10, left: 8, right: 8),
              width: 150,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    style: TextStyle(fontSize: 15, color: BaseTheme.oxfordBlue),
                  ),
                  Text(
                    price,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: BaseTheme.oxfordBlue),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
