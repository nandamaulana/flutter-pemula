import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gudang_sepatu/base/base_theme.dart';

class MyNavbarItem extends StatelessWidget {
  const MyNavbarItem(
      {Key? key,
      required final this.isActive,
      required this.icon,
      required this.label,
      this.itemCount = 5,
      required this.onTap})
      : super(key: key);

  final bool isActive;
  final String icon;
  final String label;
  final int itemCount;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    var _mediaQuery = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        onTap.call();
      },
      child: Container(
        padding: const EdgeInsets.only(bottom: 2, left: 2, right: 2),
        width: (_mediaQuery.width / this.itemCount) - 4,
        child: Stack(
          children: [
            Visibility(
              visible: isActive,
              child: Container(
                height: 68,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: BaseTheme.primaryColor),
              ),
            ),
            Container(
              height: 60,
              width: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: SvgPicture.asset(
                        icon,
                        color: isActive ? Colors.white : BaseTheme.grayChateau,
                      ),
                    ),
                    Container(
                      child: isActive
                          ? Container(
                              height: 3,
                              width: 30,
                              margin: EdgeInsets.only(bottom: 6, top: 6),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                            )
                          : Container(
                              padding: EdgeInsets.only(bottom: 5),
                              child: Text(
                                label.toUpperCase(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: isActive
                                        ? Colors.white
                                        : BaseTheme.grayChateau),
                              ),
                            ),
                    )
                  ]),
            )
          ],
        ),
      ),
    );
  }
}
