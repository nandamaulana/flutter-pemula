import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gudang_sepatu/base/base_theme.dart';
import 'package:gudang_sepatu/data/model/shoes.dart';
import 'package:gudang_sepatu/ui/page/detail_shoes_page.dart';
import 'package:gudang_sepatu/ui/widget/shoes_card_item.dart';

class DiscoverPage extends StatefulWidget {
  const DiscoverPage({
    Key? key,
    required List<Shoes> listShoes,
  })  : _listShoes = listShoes,
        super(key: key);

  final List<Shoes> _listShoes;

  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 24, left: 18, right: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 24),
              child: Text(
                "Discover",
                style: TextStyle(
                    color: BaseTheme.oxfordBlue,
                    fontWeight: FontWeight.w500,
                    fontSize: 36),
              ),
            ),
            Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Sort By Price".toUpperCase(),
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  SvgPicture.asset("assets/svg/filter_alt_fill.svg")
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 4),
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200,
                        mainAxisExtent: 200,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 0),
                    itemCount: widget._listShoes.length,
                    itemBuilder: (buildContext, index) {
                      Shoes item = widget._listShoes[index];
                      return ShoesCardItem(
                        image: item.image,
                        title: item.title,
                        price: "\$${item.price}",
                        onClick: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return DetailShoes();
                          }));
                        },
                      );
                    }),
              ),
            )
          ],
        ));
  }
}
