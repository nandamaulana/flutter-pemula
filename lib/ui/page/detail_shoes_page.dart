import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:math' as math;

import 'package:gudang_sepatu/base/base_theme.dart';

class DetailShoes extends StatefulWidget {
  const DetailShoes({Key? key}) : super(key: key);
  @override
  _DetailShoesState createState() => _DetailShoesState();
}

class _DetailShoesState extends State<DetailShoes> {
  late PageController thumbnailController;

  int thumbnailIndex = 0;
  List<String> imageList = [
    "assets/images/shoes_1.png",
    "assets/images/shoes_2.png",
    "assets/images/shoes_3.png",
    "assets/images/shoes_4.png",
  ];

  @override
  void initState() {
    super.initState();
    thumbnailController = PageController(keepPage: true, initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Stack(
                        children: [
                          LayoutBuilder(
                            builder: (buildContext, boxConstraints) {
                              return Container(
                                width: boxConstraints.maxWidth,
                                height: boxConstraints.maxWidth - 18,
                                child: PageView(
                                    controller: thumbnailController,
                                    onPageChanged: (position) {
                                      setState(() {
                                        thumbnailIndex = position;
                                      });
                                    },
                                    children: imageList
                                        .map(
                                          (item) => Container(
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(15)),
                                                image: DecorationImage(
                                                    image: AssetImage(item),
                                                    fit: BoxFit.fill)),
                                          ),
                                        )
                                        .toList()),
                              );
                            },
                          ),
                          Positioned(
                            bottom: 14,
                            left: 14,
                            child: Visibility(
                              visible: thumbnailIndex != 0,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    thumbnailIndex--;
                                  });
                                  thumbnailController.animateToPage(
                                      thumbnailIndex,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.linear);
                                },
                                child: RotatedBox(
                                  quarterTurns: 2,
                                  child: SvgPicture.asset(
                                    "assets/svg/playbutton.svg",
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 14,
                            right: 14,
                            child: Visibility(
                              visible: thumbnailIndex != (imageList.length - 1),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    thumbnailIndex++;
                                  });
                                  thumbnailController.animateToPage(
                                      thumbnailIndex,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.linear);
                                },
                                child: SvgPicture.asset(
                                    "assets/svg/playbutton.svg"),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: imageList.map(
                            (item) {
                              var index = imageList.indexOf(item);
                              return InkWell(
                                onTap: () {
                                  setState(() {
                                    thumbnailIndex = index;
                                  });
                                  thumbnailController.animateToPage(
                                      thumbnailIndex,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.linear);
                                },
                                child: Container(
                                  height: 80,
                                  width: 80,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: thumbnailIndex == index
                                              ? Theme.of(context).primaryColor
                                              : Colors.transparent),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(15)),
                                      image: DecorationImage(
                                          image: AssetImage(item),
                                          fit: BoxFit.fill)),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 16, right: 50),
                        child: Text("Zoom Freak 2 ‘Play for The Future’",
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w500,
                                color: BaseTheme.oxfordBlue)),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 16),
                        child: Text(
                          "Description",
                          style: TextStyle(
                              fontSize: 16,
                              color: BaseTheme.oxfordBlue,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 50),
                        child: Text(
                          "Giannis and his brothers used to share a pair of basketball shoes, so they all had something to wear when it was their turn to play. Today, the Antetokounbros all have their own sneakers, but they never forgot what they",
                          style: TextStyle(
                              fontSize: 16,
                              height: 1.2,
                              color: BaseTheme.grayChateau,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 16),
                color: Colors.white,
                child: ElevatedButton(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SvgPicture.asset("assets/svg/bag_border.svg"),
                          Text(
                            "Add to Chart",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          ),
                          Container(
                            width: 1,
                            height: 20,
                            color: Colors.white,
                          ),
                          Text(
                            "\$120",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          ),
                        ]),
                  ),
                  onPressed: () {},
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
