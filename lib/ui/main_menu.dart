import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gudang_sepatu/base/base_theme.dart';
import 'package:gudang_sepatu/data/model/shoes.dart';
import 'package:gudang_sepatu/ui/page/discover_page.dart';
import 'package:gudang_sepatu/ui/widget/my_navbar_item.dart';
import 'package:gudang_sepatu/ui/widget/shoes_card_item.dart';

class MainMenu extends StatefulWidget {
  const MainMenu({Key? key}) : super(key: key);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  int _currentPositionTab = 0;
  List<MenuNavBar> _navbarMenu = [
    MenuNavBar(label: "Home", icon: "assets/svg/home.svg"),
    MenuNavBar(label: "Search", icon: "assets/svg/search.svg"),
    MenuNavBar(label: "Card", icon: "assets/svg/bag.svg"),
    MenuNavBar(label: "Favorite", icon: "assets/svg/star.svg"),
    MenuNavBar(label: "Profile", icon: "assets/svg/user.svg"),
  ];

  List<Shoes> _listShoes = [
    Shoes(image: "assets/images/shoes_1.png", title: "Shoes 1", price: "100"),
    Shoes(image: "assets/images/shoes_2.png", title: "Shoes 2", price: "200"),
    Shoes(image: "assets/images/shoes_3.png", title: "Shoes 3", price: "300"),
    Shoes(image: "assets/images/shoes_4.png", title: "Shoes 4", price: "400"),
    Shoes(image: "assets/images/shoes_1.png", title: "Shoes 5", price: "110"),
    Shoes(image: "assets/images/shoes_2.png", title: "Shoes 6", price: "120"),
    Shoes(image: "assets/images/shoes_3.png", title: "Shoes 7", price: "130"),
    Shoes(image: "assets/images/shoes_4.png", title: "Shoes 8", price: "140"),
  ];

  late PageController pageController;
  @override
  void initState() {
    super.initState();
    pageController = PageController(keepPage: true, initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    Size _mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
        body: PageView(
          controller: pageController,
          onPageChanged: (position) {
            setState(() {
              _currentPositionTab = position;
            });
          },
          children: [
            DiscoverPage(listShoes: _listShoes),
            DiscoverPage(listShoes: _listShoes),
            DiscoverPage(listShoes: _listShoes),
            DiscoverPage(listShoes: _listShoes),
            DiscoverPage(listShoes: _listShoes),
          ],
        ),
        bottomNavigationBar: Container(
          width: double.infinity,
          height: 68,
          child: Stack(
            alignment: Alignment.bottomLeft,
            children: [
              Container(
                width: double.infinity,
                height: 60,
                color: Colors.white,
              ),
              Row(
                children: _navbarMenu.map((item) {
                  var index = _navbarMenu.indexOf(item);
                  return MyNavbarItem(
                    isActive: index == _currentPositionTab,
                    icon: item.icon,
                    label: item.label,
                    onTap: () {
                      setState(() {
                        _currentPositionTab = index;
                      });
                      pageController.animateToPage(_currentPositionTab,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.linear);
                    },
                  );
                }).toList(),
              )
            ],
          ),
        ));
  }
}

class MenuNavBar {
  int position = 0;
  String label = "";
  String icon = "assets/svg/home.svg";
  MenuNavBar({label, icon, position}) {
    this.label = label;
    this.icon = icon;
  }
}
