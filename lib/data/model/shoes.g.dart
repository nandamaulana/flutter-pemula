// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shoes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shoes _$ShoesFromJson(Map<String, dynamic> json) {
  return Shoes(
    image: json['image'] as String,
    title: json['title'] as String,
    price: json['price'] as String,
  );
}

Map<String, dynamic> _$ShoesToJson(Shoes instance) => <String, dynamic>{
      'image': instance.image,
      'title': instance.title,
      'price': instance.price,
    };
