import 'package:json_annotation/json_annotation.dart';

part 'shoes.g.dart';

@JsonSerializable()
class Shoes {
  Shoes({required this.image, required this.title, required this.price});
  String image;
  String title;
  String price;

  factory Shoes.fromJson(Map<String, dynamic> json) => _$ShoesFromJson(json);

  Map<String, dynamic> toJson() => _$ShoesToJson(this);
}
